import pygame as py  # importation du module pygame
from tkinter import*  # importation du module tkinter (pour PhotoImage, ...)
import tkinter as tk  # import Tkinter
from timeit import default_timer
import time as tt
from random import randint
import sys

# permet de jouer un son avec pygame
py.init()
py.mixer.init()


# Fenêtre principale
window = tk.Tk()
window.title("Pian-Apéro")  # Definition du nom de la fenetre
window.geometry('1920x1080')  # Taille de ma fenêtre
window.overrideredirect(1) #Masquer la decoration de fenetre
#window.resizable(width=False, height=False)  # pour ne pas pouvoir ajuster la fenêtre


# background image
background = PhotoImage(file=r'assets/fonddefilement.png')
background_hardcore = PhotoImage(file="assets/fondefilementhardcore.png")
labelfond = Label(window, image=background)
labelfond.place(relwidth=1, relheight=1)

## Liste des photos pour changement de fond
my_background = [background, background_hardcore]
which_background = 0  ## INDEX, pour la liste, mes_photos.



# Zones de placements des éléments
Zone_PIANO = Frame(width=30, height=100).place(height=500, width=880, x=520, y=430)
Zone_Outils = Canvas(height = 130, width = 878, bg = "#414141", highlightthickness=0, relief='ridge')


# images liés aux touches
white_keys_png = PhotoImage(file='assets/testv15.png')
black_keys_png = PhotoImage(file='assets/testBv15.png')
pressed_white_keys_png = PhotoImage(file='assets/WK_test_B.png')
pressed_black_keys_png = PhotoImage(file='assets/BK_test_B.png')

# images liés aux outils
record = PhotoImage(file="assets/rectest_off.png")
recordStop = PhotoImage(file="assets/rectest_on.png")
Play = PhotoImage(file="assets/Play.png")
Stop = PhotoImage(file="assets/Stop.png")
time = PhotoImage(file="assets/time.png")
hardcore = PhotoImage(file="assets/hardcore.png")
hardrape = PhotoImage(file="assets/hardrape2.png")
hautdupiano = PhotoImage(file = r'assets\\haut du piaano.png')
exit = PhotoImage(file="assets/exit_b.png")

# images liés aux notes d'animations
red = PhotoImage(file = "assets/new_note.png")
yellow = PhotoImage(file = "assets/new_yellow.png")
green = PhotoImage(file = "assets/new_green.png")
purple = PhotoImage(file = "assets/new_purple.png")
blue = PhotoImage(file = "assets/new_blue.png")

## Liste des images pour le clignotement de record
my_record = [record, recordStop]
which_record = 0


# Liste des touches

def fullmaj():
    global keyboard_w, keyboard_b
    keyboard_w = ['Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'percent']
    keyboard_b = ['Z', None, 'R', 'T', None, 'U', 'I', 'O', None, 'Multi_key', None]


def normalkeyboard():
    global keyboard_w, keyboard_b
    keyboard_w = ['q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'ugrave']
    keyboard_b = ['z', None, 'r', 't', None, 'u', 'i', 'o', None, 'Multi_key', None]

fullmaj_pressed = [1]

def keyboard_touches(arg):
    global fullmaj_pressed
    if fullmaj_pressed[0] == 0:
        normalkeyboard()
        fullmaj_pressed.clear()
        fullmaj_pressed.append(1)
    else:
        fullmaj()
        fullmaj_pressed.clear()
        fullmaj_pressed.append(0)


# touches blanches
keyboard_w = ['q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'ugrave']

# touches noires
keyboard_b = ['z', None, 'r', 't', None, 'u', 'i', 'o',None, 'Multi_key', None]


# touches pressées
#def f(event):
    #t = event.keysym
    #print("Touche pressée :", t)
    # touches blanches
    #if t in keyboard_w:
     #   window.bind("<q>", jouerNote(w_note[keyboard_w.index(t)]))
        #jouerNote(w_note[keyboard_w.index(t)])
    # touches noires
    #if t in keyboard_b:
    #    jouerNote(b_note[keyboard_b.index(t)])

# .index permettant de prendre le champ associé à la touche (ex: 'd' = keyboard_w[2] et 'DO' = w_note[2])
# cela permet ainsi d'associer ces deux tableaux et donc de lier une touche à une note et donc un son



# CREATION DES TOUCHES

white_keys = 11
black_keys = [1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0]
w_note = ['LAGRAVE', 'SIGRAVE', 'DO', 'RE', 'MI', 'FA', 'SOL', 'LA', 'SI', 'DOAIGUE', 'REAIGUE']
b_note = ['LAGRAVE - SIGRAVE', None, 'DO - RE', 'RE - MI', None, 'FA - SOL', 'SOL - LA', 'LA - SI', None, 'DOAIGUE - REAIGUE', None]

#imaage = PhotoImage(file = "assets/touuche.png")

whiteButton = []
blackButton = [None, None, None, None, None, None, None, None, None, None, None]

def press(arg):
    t = arg.keysym
    if t in keyboard_w:
        ind = keyboard_w.index(t)
        whiteButton[ind].config(image=pressed_white_keys_png)
        jouerNote(w_note[ind])
    if t in keyboard_b:
        ind = keyboard_b.index(t)
        blackButton[ind].config(image=pressed_black_keys_png)
        jouerNote(b_note[ind])


def released(arg):
    t = arg.keysym
    if t in keyboard_w:
        ind = keyboard_w.index(t)
        whiteButton[ind].config(image=white_keys_png)
    if t in keyboard_b:
        ind = keyboard_b.index(t)
        blackButton[ind].config(image=black_keys_png)


def press_button(arg):
    bb = arg.widget
    if bb in whiteButton:
        bb.config(image=pressed_white_keys_png)
    if bb in blackButton:
        bb.config(image=pressed_black_keys_png)


def released_button(arg):
    bb = arg.widget
    if bb in whiteButton:
        bb.config(image=white_keys_png)
    if bb in blackButton:
        bb.config(image=black_keys_png)



def keys_installation(space):
    # touches blanches
    for i in range(white_keys):
        bb = Button(Zone_PIANO, image=white_keys_png, bg="#333333", activebackground="#333333", borderwidth=0,
                  cursor="hand2", command=lambda i=i: jouerNote(w_note[i]))
        whiteButton.append(bb)
        bb.place(height=500, width=80,
               x=520 + (i * space), y=430)

    # touches noires
    for i in range(white_keys - 1):
        if black_keys[i]:
           bb = Button(Zone_PIANO, image=black_keys_png, bg="#FFFFFF", activebackground='#FFFFFF', borderwidth=0,
                      cursor="hand2", command=lambda i=i: jouerNote(b_note[i]))
           blackButton[i] = bb
           bb.place(height=270, width=50,
                  x=575 + (i * space), y=430)


work = False

def startChrono():
    global startt, work
    startt = int(tt.time())
    chrono()


running = False
def chrono():
    global startt, isRecordEnabled, running, now, str_time
    now = int(tt.time())-startt
    minutes, seconds = divmod(now, 60)
    str_time = "%02d:%02d" % (minutes, seconds)
    Zone_Outils.itemconfigure(text_clock, text=str_time, font=("", 15))
    if running is False:
        window.after(1000, chrono)
    else:
        now = 0
        minutes, seconds = 0, 0
        running = False




# définition du bouton hardcore et de quand l'activer
def startHardcore():
    randomx = randint(0, 20)
    y = randint(0, 20)
    if randomx == y:
        labelfond.config(image=background_hardcore)
        tryy = py.mixer.Sound("assets/prout_rapide.wav")
        tryy.play()
        change_background()




# o : pour vérif si bouton activé ou non
o = 0
def LunchHardcore():
    global o
    hc.config(image = hardrape)
    if o == 1:
        hc.config(image = hardcore)
        o = 0
    else:
        o = 1



compteur = 0
def change_background():
    global which_background, compteur
    which_background += 1

    if which_background >= len(my_background):
        which_background = 0  ## Remet à zéro

    labelfond.config(image=my_background[which_background])

    compteur += 1
    if compteur < 2:
        window.after(150, change_background)

    if compteur == 2:
        compteur = 0


# record / stoprecord
def recordtest():
    global Rec, isRecording, recordStartingTime
    startChrono() #lancement du chrono
    launchRecord()# lancement du bouton rec
    recordStartingTime = tt.time()
    Rec = []
    isRecording = True

# play / stop
def ReplaySon():
    global Rec, isRecording, isRecordEnabled, running, str_time, memory
    if isRecordEnabled is True: # permet de stopper l'animation du bouton play
        isRecordEnabled = False
        record_b.config(image=record)
    isRecording = False
    running = True
    i = 0

    try:
        while i < len(Rec):
            sontest = Rec[i][1]
            sontest2 = notes[sontest]
            tempo = Rec[i][0] - Rec[i - 1][0]
            print(tempo)
            tempo2 = Rec[i][0]
            if tempo > 0:
                tt.sleep(tempo)
            else:
                tt.sleep(tempo2)
            sontest2.play()
            i += 1
    except:
        pass




isRecordEnabled = False
recordLogoState = False

def change_record():
    global isRecordEnabled, recordLogoState
    if isRecordEnabled:
        if recordLogoState:
            record_b.config(image=record)
        else:
            record_b.config(image=recordStop)
        recordLogoState = not recordLogoState
        window.after(600, change_record)


def launchRecord():
    global isRecordEnabled
    isRecordEnabled = not isRecordEnabled
    record_b.config(image=record)
    change_record()




def exit_piano():
    sys.exit()


def exit_escape(event):
    window.quit()


# son des touches du piano
# touches blanches
notes = {
    'LAGRAVE': py.mixer.Sound("assets/Blanc_1.wav"),
    'SIGRAVE': py.mixer.Sound("assets/Blanc_2.wav"),
    'DO': py.mixer.Sound("assets/Blanc_3.wav"),
    'RE': py.mixer.Sound("assets/Blanc_4.wav"),
    'MI': py.mixer.Sound("assets/Blanc_5.wav"),
    'FA': py.mixer.Sound("assets/Blanc_6.wav"),
    'SOL': py.mixer.Sound("assets/Blanc_7.wav"),
    'LA': py.mixer.Sound("assets/Blanc_8.wav"),
    'SI': py.mixer.Sound("assets/Blanc_9.wav"),
    'DOAIGUE': py.mixer.Sound("assets/Blanc_10.wav"),
    'REAIGUE': py.mixer.Sound("assets/Blanc_11.wav"),

    # touches noires
    'LAGRAVE - SIGRAVE': py.mixer.Sound("assets/Noir_1.wav"),
    'DO - RE': py.mixer.Sound("assets/Noir_2.wav"),
    'RE - MI': py.mixer.Sound("assets/Noir_3.wav"),
    'FA - SOL': py.mixer.Sound("assets/Noir_4.wav"),
    'SOL - LA': py.mixer.Sound("assets/Noir_5.wav"),
    'LA - SI': py.mixer.Sound("assets/Noir_6.wav"),
    'DOAIGUE - REAIGUE': py.mixer.Sound("assets/Noir_7.wav"),
}



def animateMusicAnimation(canvas, y_cord):
    y_cord -= 20 # déplacement
    canvas.place_configure(y=y_cord)  # configurer la position de l'image
    if y_cord > -1: #ne plus continuer l'aciton après que l'image soit sortie du cadre
        window.after(25, animateMusicAnimation, canvas, y_cord) #répéter l'action
    else:
        canvas.destroy()



def addMusicAnimation():
    global picture_off, y_cord, x_cord
    free_place_list_x = [0, 1400, 519, 1980]
    free_place_list_y = [0, 0, 930, 1080]
    choice = randint(0, 1)
    x_cord = (randint(free_place_list_x[choice], free_place_list_x[choice + 2])) - 50
    y_cord = (randint(free_place_list_y[choice], free_place_list_y[choice + 2])) - 50
    choice_color = randint(0, 4)
    list_of_choice = [red, yellow, green, purple, blue]
    loto = list_of_choice[choice_color]
    canvas = Label(window, bg="#333333",image = loto, border = 0)
    canvas.place(height=75, width=75, x=x_cord, y=y_cord)
    y_cord -= 20 # déplacement
    canvas.place_configure(y = y_cord) #configurer la position de l'image
    if y_cord > -100: #ne plus continuer l'aciton après que l'image soit sortie du cadre
        animateMusicAnimation(canvas, y_cord)




# jouer les notes
isRecording = False
recordNote = []
recordStartingTime = tt.time()
def jouerNote(note):
    global o
    if o == 1:
        startHardcore()
    s = notes[note]
    s.play()
    if isRecording:
        Rec.append([tt.time() - recordStartingTime,note])
    addMusicAnimation()




# --- Les Outils --- #

# Table des outils
Zone_Outils.create_image(439, 0,image = hautdupiano, anchor = N)


# Bouttons des outils
# record / stoprecord
record_b = Button(Zone_Outils, image = record, height = 120, width = 120, bg="#242424", activebackground = "#242424" ,cursor="hand2", borderwidth=0, command = recordtest)
record_b.place(x = 20, y = 0)
tk.Button(Zone_Outils, image = recordStop, height = 120, width = 120, bg = "#242424", activebackground = "#242424" ,cursor="hand2", borderwidth=0, command=None)

# play / stop
tk.Button(Zone_Outils, image = Play , height = 120, width = 120, bg = "#242424", activebackground = "#242424" , borderwidth=0,cursor="hand2", command=ReplaySon).place(x = 165, y = 0)
tk.Button(Zone_Outils, image = Stop, height = 120, width = 120, bg = "#242424", activebackground = "#242424" , borderwidth=0,cursor="hand2", command=None)

# time
Zone_Outils.create_image(375, 65, image = time)
start = default_timer()
text_clock = Zone_Outils.create_text(381, 65, fill="#2DCBBA")


# hardcore
hc = Button(Zone_Outils, image = hardcore, height = 110, width = 200, bg = "#242424", activebackground = "#242424" ,cursor="hand2", borderwidth=0, command=LunchHardcore)
hc.place(x = 675, y = 5)


# placement et apparition des outils
Zone_Outils.place(x = 521, y = 300)


# exit piano
exit_b = Button(window, image = exit, background = "#333333", activebackground = "#333333",cursor="hand2", height = 120, width = 120, borderwidth=0, command=exit_piano)
exit_b.place(x = 10, y = 948)


keys_installation(80)


window.bind('<KeyRelease>', released)
window.bind('<KeyPress>', press)
window.bind('<ButtonRelease>', released_button)
window.bind('<Button>', press_button)
window.bind("<Caps_Lock>", keyboard_touches)
window.bind("<Escape>", exit_escape)

window.mainloop()  # pour faire tourner le programme