from django.db import models

from django.contrib.auth.models import User


class UserData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE) #Lien fort vers l'utilisateur de Django
    pdp = models.ImageField(upload_to='pdp') #Photo de profil

    def __str__(self):
        return self.user.username


class UserScore(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE) #Lien fort vers l'utilisateur de Django

    games = models.CharField(max_length=20) # Nom du jeu

    score = models.IntegerField() # Score
    time = models.IntegerField() # Temps

    hard = models.BooleanField() # Difficulté
