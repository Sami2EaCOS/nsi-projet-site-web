let currentPawn = "croixMORPION";



function pawnPlacement() {
    let signBox = document.querySelectorAll("*:hover")[6];
    let slotFree = checkSlot(signBox);
    let caseId = signBox.id;

    document.querySelectorAll("#" + caseId + " > #" + slotFree)[0].style.backgroundImage = "url('../../static/img/" + currentPawn + ".png')"

    playerSwap()
    document.getElementById("previewPlayer").src= "../../static/img/" + currentPawn + ".png"
}

function playerSwap() {
    if (currentPawn === "croixMORPION"){
        currentPawn = "rondMORPION";
    } else {
        currentPawn = "croixMORPION";
    }

}

function checkSlot (signBox) {
    let nodesMORPION = signBox.childNodes;
    let reversedNodesMORPION = [];
    let slotFree = null;

    for(let i = nodesMORPION.length -1; i >= 0; i--) {
            if (nodesMORPION[i].nodeName.toLowerCase() == 'div') {
               reversedNodesMORPION.push(nodesMORPION[i]);
         }
    }

    reversedNodesMORPION.forEach(mor => {
        if (mor.style.backgroundImage == "") {
            if(slotFree == null) {
                slotFree = mor.id;
            }
        }
    })

    return slotFree;
}
function checkPlayer() {

    let playerSign = document.getElementById('playerOneSign').value;
    let buttonMor = document.getElementById('generateMORPION');
    currentPawn = playerSign;

    if (playerSign == "Choix du joueur 1") {
        buttonMor.disabled = true;
   } else {

    buttonMor.disabled= false;
    }
}

function launchMORPION() {
    document.getElementById('playerSign').style.display = 'none';
    document.getElementById('gameMorpionContainer').style.display = 'block';
    document.getElementById('previewSign').style.display = 'block';
    document.getElementById("previewPlayer").src= "../../static/img/" + currentPawn + ".png"
    }
