let currentPlayer = "rougeP4";



function tokenPlacement() {
    let tokenCol = document.querySelectorAll("*:hover")[6];
    let freeSlot = slotCheck(tokenCol);
    let tokenColId = tokenCol.id;

    document.querySelectorAll("#" + tokenColId + " > #" + freeSlot)[0].style.backgroundImage = "url('../../static/img/" + currentPlayer + ".png')"

    swapPlayer()
    document.getElementById("playerPreview").src= "../../static/img/" + currentPlayer + ".png"
}

function swapPlayer() {
    if (currentPlayer === "rougeP4"){
        currentPlayer = "jauneP4";
    } else {
        currentPlayer = "rougeP4";
    }

}

function slotCheck (tokenCol) {
    let nodes = tokenCol.childNodes;
    let reversedNodes = [];
    let freeSlot = null;

    for(let i = nodes.length -1; i >= 0; i--) {
            if (nodes[i].nodeName.toLowerCase() == 'div') {
               reversedNodes.push(nodes[i]);
         }
    }

    reversedNodes.forEach(e => {
        if (e.style.backgroundImage == "") {
            if(freeSlot == null) {
                freeSlot = e.id;
            }
        }
    })

    return freeSlot;
}
function playerCheck() {

    let playerColor = document.getElementById('playerOneColor').value;
    let button = document.getElementById('generateP4');
    currentPlayer = playerColor;

    if (playerColor == "Choix du joueur 1") {
        button.disabled = true;
   } else {

    button.disabled= false;
    }
}

function launchP4() {
    document.getElementById('playerColor').style.display = 'none';
    document.getElementById('gameP4Container').style.display = 'block';
    document.getElementById('previewColor').style.display = 'block';
    document.getElementById("playerPreview").src= "../../static/img/" + currentPlayer + ".png"

}// verif du gagnant